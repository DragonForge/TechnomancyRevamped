#priority 1149
#loader contenttweaker

import mods.contenttweaker.Material;
import mods.contenttweaker.MaterialPartData;

import scripts.contenttweaker.base.materials;

var materailsForDefaultParts as Material[] = [
	materials.thaumium,
	materials.void,
	materials.alchemicalbrass
];

var defaultPartNames as string[] = [
	"gear"
];

for i, metal in materailsForDefaultParts {
	metal.registerParts(defaultPartNames);
}

/*
//Invar
var invarParts as string[] = [
	"block"
];
materials.invar.registerParts(invarParts);
*/

/*
var defaultPartNames as string[] = [
	"gear",
	"plate",
	"rod",
	"bolt",
	"block"
];

for i, metal in materailsForDefaultParts {
	metal.registerParts(defaultPartNames);
}
*/