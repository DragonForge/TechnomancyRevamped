#priority 1150
#loader contenttweaker

import mods.contenttweaker.Color;
import mods.contenttweaker.Material;
import mods.contenttweaker.MaterialPartData;
import mods.contenttweaker.MaterialSystem;

static materials as Material[string] = {
	"thaumium": MaterialSystem.getMaterialBuilder().setName("Thaumium").setColor(5325692).build(),
	"void": MaterialSystem.getMaterialBuilder().setName("Void").setColor(2297916).build(),
	"alchemicalbrass": MaterialSystem.getMaterialBuilder().setName("Alchemical Brass").setColor(10580261).build()
};
